package org.kshrd.posgresqlfunctionspringdemo.controller;

import org.kshrd.posgresqlfunctionspringdemo.model.Product;
import org.kshrd.posgresqlfunctionspringdemo.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Controller
// Controlller + ResponBody
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/product/{productName}")
    public String getProductList(@PathVariable("productName") String productName, ModelMap modelMap) {

        List<Product> productList = this.productService.productList(productName);
        modelMap.addAttribute("products", productList);

        return "index";
    }

    @GetMapping("/api/product/{productName}")
    @ResponseBody
    public List<Product> getProductList(@PathVariable("productName") String productName) {
        List<Product> productList = this.productService.productList(productName);
        return productList;

    }


}
