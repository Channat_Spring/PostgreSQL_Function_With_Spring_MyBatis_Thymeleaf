package org.kshrd.posgresqlfunctionspringdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PosgresqlFunctionSpringDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(PosgresqlFunctionSpringDemoApplication.class, args);
    }
}
