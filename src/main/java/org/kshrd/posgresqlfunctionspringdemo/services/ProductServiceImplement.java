package org.kshrd.posgresqlfunctionspringdemo.services;

import org.kshrd.posgresqlfunctionspringdemo.model.Product;
import org.kshrd.posgresqlfunctionspringdemo.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImplement implements ProductService {

    ProductRepository productRepository;

    @Autowired
    public ProductServiceImplement(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> productList(String productName) {
        return productRepository.productList(productName);
    }
}
