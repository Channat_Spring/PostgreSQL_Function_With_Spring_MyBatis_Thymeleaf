package org.kshrd.posgresqlfunctionspringdemo.services;

import org.kshrd.posgresqlfunctionspringdemo.model.Product;

import java.util.List;

public interface ProductService {

    List<Product> productList(String productName);

}
