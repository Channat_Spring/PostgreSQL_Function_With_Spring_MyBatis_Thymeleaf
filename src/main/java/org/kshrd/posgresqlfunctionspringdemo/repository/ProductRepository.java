package org.kshrd.posgresqlfunctionspringdemo.repository;

import org.apache.ibatis.annotations.*;
import org.kshrd.posgresqlfunctionspringdemo.model.Product;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.Mapping;

import java.util.List;

@Repository

public interface ProductRepository {

    @Select("SELECT * FROM func_get_product(#{productName})")
    @Results({
            @Result(column = "r_id", property = "id"),
            @Result(column = "r_product_name", property = "name"),
            @Result(column = "r_category_name", property = "category.name")
    })

    List<Product> productList( String productName);

}
